#!/usr/bin/perl
use strict;
use warnings;
use 5.010;
use Data::Dumper qw(Dumper);

#my $player = shift;
my $in_file = shift;

die "MPlayer2 EDL file to play is not specified.\n" unless defined $in_file;
#open (my $in, "<", "dscn3895.avi.edl-mp2")  or die "Can't open input file: $!";
open (my $in, "<", $in_file)  or die "Can't open input file $in_file: $!";

my @mp2_lines = <$in>;
my @out_lines;

convert();

sub convert {
	die "Not an MPlayer 2 EDL file: $!" unless shift @mp2_lines eq "mplayer EDL file, version 2\n";

	my %vid_names;
	for (@mp2_lines) {
		chomp;
		my $source_line = $_;
		push @out_lines, "#$source_line";

		if ($source_line =~ /^< (\S+) (.+)$/) {   # a filename alias definition
			my $fnalias = $1;
			my $actualfilename = $2;
			if ($actualfilename =~ m/[=%,;\n]/)
			{
				my $fnlength = length($actualfilename);
				$actualfilename = "%$fnlength%$actualfilename";
			};
			$vid_names{$fnalias} = $actualfilename;
		}
		elsif ($source_line =~ /^(\S+) ([\d\.]+)-([\d\.]+)$/) {  # a time range with end timestamp
			my $fnalias = $1;
			my $timestamp_start = $2;
			my $timestamp_end = $3;
			my $length = $timestamp_end - $timestamp_start;
			push @out_lines, "$vid_names{$fnalias},start=$timestamp_start,length=$length";

		}
		elsif ($source_line =~ /^(\S+) ([\d\.]+)\+([\d\.]+)$/) {  # a time range with duration
			my $fnalias = $1;
			my $timestamp_start = $2;
			my $length = $3;
			push @out_lines, "$vid_names{$1},start=$timestamp_start,length=$length";

		};

	};

}

# Print the file version
#my $out_lines_for_file = "# mpv EDL v0\n" . join("\n", @out_lines) . "\n";
#print $out_lines_for_file;

# Print the detailed version
#say Dumper \@out_lines;


# Run the command line version
my @out_lines_for_cli;
for (@out_lines) { push @out_lines_for_cli, $_ unless /^#/ };
my $cliedl = "edl://" . join(';', @out_lines_for_cli) . ";";

system('mpv', '--load-scripts=no', $cliedl);


