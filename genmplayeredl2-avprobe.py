#!/usr/bin/python3

import subprocess
import argparse
import json
import os.path
import sys

parser = argparse.ArgumentParser()
parser.add_argument('movie', nargs=1)

args = parser.parse_args()

s_filename = args.movie[0]

cl_default = ["/usr/bin/avprobe", "-of", "json"]

avout = subprocess.check_output(cl_default + ['-show_format'] + ["file:" + s_filename], universal_newlines=True)
#print(avout)

avj = json.loads(avout)


jformat = avj['format']

av_duration = jformat['duration']

edl_filename = s_filename + ".edl-mp2"

if os.path.exists(edl_filename):
    print("ERROR: {0} already exists!".format(edl_filename))
    sys.exit(1)

edlc = "mplayer EDL file, version 2\n< f {0}\nf 0-{1}\n".format(s_filename,
        av_duration)

with open(edl_filename, 'w') as f:
    f.write(edlc)

